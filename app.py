from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/")
def greeting():
    return "<h1 style='color:green'>Hello World! This site is using MPTCP</h1>"

from flask import jsonify

@app.route("/get_my_ip", methods=["GET"])
def get_my_ip():
    return jsonify({'ip': request.remote_addr}), 200

if __name__ == "__main__":
    app.run(host='0.0.0.0')
