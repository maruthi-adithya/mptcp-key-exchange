import socket
import re
import hashlib
import base64

def compute_key(key1,key2):
	hash1 = hashlib.md5(key1.encode())
	hash2 = hashlib.md5(key2.encode())
	return hash1.hexdigest()+hash2.hexdigest()

def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc))
	

print "Waiting for Connection.."

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
my_ip = s.getsockname()[0]
s.close()

#create an INET, raw socket
s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)

ipset = set()

# receive a packet
while True:
  packet = str(s.recvfrom(65565))
  ip = re.findall( r'[0-9]+(?:\.[0-9]+){3}', packet)
  for x in ip:
	if x.startswith('172.16') and x!=my_ip:
		#print x
		ipset.add(x)
  
  if len(ipset)==2:
	break

subflows = sorted(ipset)

		
print "Subflow 1 is "+subflows[0]
print "Subflow 2 is "+subflows[1]
    
port = 9300

#Receive key1 through subflow 1
key1=-1
server_socket = socket.socket()  # get instance
server_socket.bind((my_ip, port))  # bind host address and port together
server_socket.listen(2)
conn, address = server_socket.accept()  # accept new connection
#print("Connection from: " + str(address))
while True:
	key1 = conn.recv(1024).decode()
	print("Key Received is: " + str(key1))
	break

conn.close()
server_socket.close()

#Send key2 through subflow 2


server_socket = socket.socket()  # instantiate
server_socket.connect((subflows[0], port))
key2 = raw_input("Enter the key: ")  # take input
key2 = str(key2)
server_socket.send(key2.encode())
server_socket.close()

print "Key 1 is "+ key1
print "Key 2 is "+ key2


print "Final Key is " + compute_key(key1,key2)








  

           
